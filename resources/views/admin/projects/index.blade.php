@extends('admin.layouts.app')
@section('title')
    Projects
@endsection
@section('breadcrumbs')
    <li> Projects</li>
@endsection
@section('content')
    <!-- row -->
    <div class="row">
        <!-- col -->
        <div class="col-md-12">
            <!-- tile -->
            <section class="tile">
                <!-- tile header -->
                <div class="tile-header dvd dvd-btm">
                    <h1 class="custom-font">Projects</h1>
                    <ul class="controls">
                        <li>
                            <a role="button"
                               href="{{ route('admin.projects.create') }}">[<strong>{{ __('create new project') }}</strong>]</a>
                        </li>
                        <li>

                            <a role="button" class="tile-fullscreen">
                                <i class="fa fa-expand"></i> {{ __('Fullscreen') }}
                            </a>
                        </li>
                    </ul>
                </div>
                <!-- /tile header -->
                <!-- tile body -->
                <div class="tile-body">
                    <div class="tabs tabs--horisontal js-tabs">
                        <div class="tabs__content">
                            <!-- tab -->
                            <div class="tabs__tab" id="interest-earning">
                                <div class="table-block">
                                    <div class="table-block__inner">
                                        <div class="table-block__scroller">
                                            <table class="table">
                                                <thead>
                                                <tr>
                                                    <th class="col-lg-1">
                                                        <strong>Название</strong><br>
                                                    </th>
                                                    <th>
                                                        <strong>URL</strong><br>
                                                    </th>

                                                    <th>
                                                        <strong>Создан</strong>
                                                    </th>
                                                    <th>

                                                    </th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @foreach($projects as $project)
                                                    <tr>
                                                        <td>{{ $project->name }}</td>
                                                        <td>{{ $project->url }} USD</td>
                                                        <td>{{ \Carbon\Carbon::parse($project->created_at)->format('d-m-Y H:i') }}</td>
                                                        <td>
                                                            <a href="{{ route('admin.projects.edit', $project->id) }}" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i> редактировать</a>
                                                            <a href="{{ route('admin.projects.destroy', $project->id) }}" class="btn btn-xs btn-danger sure"> удалить</a>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <!-- block-->
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /tile body -->
            </section>
            <!-- /tile -->
        </div>
        <!-- /col -->
    </div>
    <!-- /row -->

@endsection