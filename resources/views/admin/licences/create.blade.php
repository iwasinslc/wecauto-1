@extends('admin/layouts.app')
@section('title')
    {{ __('Create tariff plan') }}
@endsection
@section('breadcrumbs')
    <li><a href="{{route('admin.rates.index')}}">{{ __('Tariff plans') }}</a></li>
    <li> {{ __('Create tariff plan') }}</li>
@endsection
@section('content')
    <!-- row -->
    <div class="row">
        <!-- col -->
        <div class="col-md-12">

            <!-- tile -->
            <section class="tile">

                <!-- tile header -->
                <div class="tile-header dvd dvd-btm">
                    <h1 class="custom-font">{{ __('Create tariff plan') }}</h1>
                    <ul class="controls">
                        <li>
                            <a role="button" class="tile-fullscreen">
                                <i class="fa fa-expand"></i> {{ __('Fullscreen') }}
                            </a>
                        </li>
                    </ul>
                </div>
                <!-- /tile header -->

                <!-- tile body -->
                <div class="tile-body">

                    <form class="form-horizontal" method="POST" action="{{ route('admin.licences.store') }}">
                        {{ csrf_field() }}

                        <div class="form-group">
                            <label for="name" class="col-md-4 control-label">{{ __('Name') }}</label>
                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required
                                       autofocus>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="duration" class="col-md-4 control-label">{{ __('Duration') }}</label>
                            <div class="col-md-6">
                                <input id="duration" type="text" class="form-control" name="duration" value="{{ old('duration') }}" required
                                       autofocus>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="price" class="col-md-4 control-label">{{ __('Price') }}</label>
                            <div class="col-md-6">
                                <input id="price" type="text" class="form-control" name="price" value="{{ old('price') }}" required
                                       autofocus>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="levels" class="col-md-4 control-label">{{ __('Levels') }}</label>
                            <div class="col-md-6">
                                <input id="levels" type="text" class="form-control" name="levels" value="{{ old('levels') }}" required
                                       autofocus>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="buy_amount" class="col-md-4 control-label">{{ __('Buy Amount') }}</label>
                            <div class="col-md-6">
                                <input id="buy_amount" type="text" class="form-control" name="buy_amount" value="{{ old('buy_amount') }}" required
                                       autofocus>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="sell_amount" class="col-md-4 control-label">{{ __('Sell Amount') }}</label>
                            <div class="col-md-6">
                                <input id="sell_amount" type="text" class="form-control" name="sell_amount" value="{{ old('sell_amount') }}" required
                                       autofocus>
                            </div>
                        </div>


                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Create licence') }}
                                </button>
                            </div>
                        </div>
                    </form>

                </div>
                <!-- /tile body -->

            </section>
            <!-- /tile -->

        </div>
        <!-- /col -->
    </div>
    <!-- /row -->

@endsection