@extends('admin/layouts.app')

@section('title')
    Редактировать проект
@endsection

@section('breadcrumbs')
    <li><a href="{{route('admin.licences.index')}}">Лицензии</a></li>
    <li> Редактировать лицензию "{{ $licence->name }}"</li>
@endsection

@section('content')
    <!-- row -->
    <div class="row">
        <!-- col -->
        <div class="col-md-12">

            <!-- tile -->
            <section class="tile">

                <!-- tile header -->
                <div class="tile-header dvd dvd-btm">
                    <h1 class="custom-font">Редактировать лицензию "{{ $licence->name }}"</h1>
                    <ul class="controls">
                        <li>
                            <a role="button" tabindex="0" class="tile-fullscreen">
                                <i class="fa fa-expand"></i> {{ __('Fullscreen') }}
                            </a>
                        </li>
                    </ul>
                </div>
                <!-- /tile header -->

                <!-- tile body -->
                <div class="tile-body">

                    <div class="row">
                        <div class="col-lg-6">
                            <form class="form-horizontal" action="{{ route('admin.licences.update', ['id' => $licence->id]) }}" enctype="multipart/form-data" method="POST" target="_top" style="width:100%;">
                                {{ csrf_field() }}

                                <div class="form-group">
                                    <label for="name" class="col-md-4 control-label">{{ __('Name') }}</label>
                                    <div class="col-md-6">
                                        <input id="name" type="text" class="form-control" name="name" value="{{ $licence->name }}" required
                                               autofocus>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="duration" class="col-md-4 control-label">{{ __('Duration') }}</label>
                                    <div class="col-md-6">
                                        <input id="duration" type="text" class="form-control" name="duration" value="{{ $licence->duration }}" required
                                               autofocus>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-4 control-label" for="currency">{{ __('Currency') }}</label>
                                    <div class="col-md-4">
                                        <select id="currency" name="currency_id" class="form-control">
                                            @foreach(getCurrencies() as $currency)
                                                <option value="{{ $currency['id'] }}"{{ $licence->currency_id == $currency['id'] ? ' selected' : '' }}>{{ $currency['code'] }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="price" class="col-md-4 control-label">{{ __('Price') }} USD</label>
                                    <div class="col-md-6">
                                        <input id="price" type="text" class="form-control" name="price" value="{{ $licence->price }}" required
                                               autofocus>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="buy_amount" class="col-md-4 control-label">{{ __('Buy Amount') }}</label>
                                    <div class="col-md-6">
                                        <input id="buy_amount" type="text" class="form-control" name="buy_amount" value="{{ $licence->buy_amount }}" required
                                               autofocus>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="sell_amount" class="col-md-4 control-label">{{ __('Sell Amount') }}</label>
                                    <div class="col-md-6">
                                        <input id="sell_amount" type="text" class="form-control" name="sell_amount" value="{{ $licence->sell_amount }}" required
                                               autofocus>
                                    </div>
                                </div>


                                <div class="form-group">
                                    <label for="sell_amount" class="col-md-4 control-label">{{ __('Daily Limit') }}</label>
                                    <div class="col-md-6">
                                        <input id="daily_limit" type="text" class="form-control" name="daily_limit" value="{{ $licence->daily_limit }}" required
                                               autofocus>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="sell_amount" class="col-md-4 control-label">{{ __('Deposit Auto min') }}</label>
                                    <div class="col-md-6">
                                        <input id="deposit_min" type="text" class="form-control" name="deposit_min" value="{{ $licence->deposit_min }}" required
                                               autofocus>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="sell_amount" class="col-md-4 control-label">{{ __('Deposit Moto min') }}</label>
                                    <div class="col-md-6">
                                        <input id="daily_limit" type="text" class="form-control" name="moto_min" value="{{ $licence->moto_min }}" required
                                               autofocus>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="sell_amount" class="col-md-4 control-label">{{ __('Deposit Moto Max') }}</label>
                                    <div class="col-md-6">
                                        <input id="deposit_max" type="text" class="form-control" name="deposit_max" value="{{ $licence->deposit_max }}" required
                                               autofocus>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-md-6">
                                        <button type="submit" class="btn btn-primary">
                                            Сохранить лицензию
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>

                </div>
                <!-- /tile body -->

            </section>
            <!-- /tile -->

        </div>
        <!-- /col -->
    </div>
    <!-- /row -->


@endsection