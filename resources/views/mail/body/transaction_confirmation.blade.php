@extends('mail.layouts.main')
@section('content')<p>{{ __('Click link to confirm your ' . $type) }}:</p>
<div style="word-break: break-all">
    <a href="{{route('transaction.confirm', ['type'=>$type, 'confirmation_code' => $confirmation_code])}}">
        <b>{{route('transaction.confirm', ['type'=>$type, 'confirmation_code' => $confirmation_code])}}</b>
    </a>
</div>
@endsection