<?php
namespace App\Http\Controllers\Customer;

use App\Http\Controllers\Controller;
use App\Models\Licences;

class AboutUsController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $licences = Licences::orderBy('price')->get();
        return view('customer.about-us', [
            'licences'=>$licences,
        ]);
    }
}
