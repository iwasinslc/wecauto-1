<?php
namespace App\Http\Controllers\Telegram\account_bot\Withdraw;

use App\Helpers\Constants;
use App\Http\Controllers\Controller;
use App\Models\Rate;
use App\Models\Telegram\TelegramBotEvents;
use App\Models\Telegram\TelegramBotMessages;
use App\Models\Telegram\TelegramBots;
use App\Models\Telegram\TelegramBotScopes;
use App\Models\Telegram\TelegramUsers;
use App\Models\Telegram\TelegramWebhooks;
use App\Models\Transaction;
use App\Models\TransactionType;
use App\Models\Wallet;
use App\Modules\Messangers\TelegramModule;

class SumController extends Controller
{
    /**
     * @param TelegramWebhooks $webhook
     * @param TelegramBots $bot
     * @param TelegramBotScopes $scope
     * @param TelegramUsers $telegramUser
     * @param TelegramBotEvents $event
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     * @throws \Throwable
     */
    public function index(TelegramWebhooks $webhook,
                          TelegramBots $bot,
                          TelegramBotScopes $scope,
                          TelegramUsers $telegramUser,
                          TelegramBotEvents $event)
    {
        TelegramModule::setLanguageLocale($telegramUser->language);

//        $scope = TelegramBotScopes::where('command', 'create_depo_wallet')
//            ->where('bot_keyword', $bot->keyword)
//            ->first();

        preg_match('/withdraw\_wallet '.Constants::UUID_REGEX.'/', $event->text, $data);


        if (isset($data[1]))
        {
            $wallet_id = $data[1];
        }
        else {
            return response('ok');
        }


        $user = $telegramUser->user;

        $wallet = $user->wallets()->find($wallet_id);

        if (null === $wallet) {
            throw new \Exception('Wallet can not be found.');
        }




        if (empty($wallet->external)) {
            $error = __('You wallet address is empty. Please, fill it in the settings.');
            $this->validationFailed($webhook, $bot, $scope, $telegramUser, $event, $error);
            return response('ok');
        }

        cache()->put('withdraw_data'.$user->id, [
            'wallet_id'=>$wallet_id
        ] , 30 );


        $message = view('telegram.account_bot.rates.sum', [
            'webhook'      => $webhook,
            'bot'          => $bot,
            'scope'        => $scope,
            'telegramUser' => $telegramUser,
            'event'        => $event,
        ])->render();

        if (config('app.env') == 'develop') {
            \Log::info('Prepared VIEW and message for bot:<hr>'.$message);
        }


        try {
            $telegramInstance = new TelegramModule($bot->keyword);
            $telegramInstance->sendMessage($event->chat_id,
                $message,
                'HTML',
                true,
                false,
                null,
                null,
                $scope,
                'inline_keyboard');
        } catch (\Exception $e) {
            \Log::error($e->getMessage());
            return response('ok');
        }




        return response('ok');
    }


    /**
     * @param TelegramWebhooks $webhook
     * @param TelegramBots $bot
     * @param TelegramBotScopes $scope
     * @param TelegramUsers $telegramUser
     * @param TelegramBotEvents $event
     * @param TelegramBotMessages $userMessage
     * @param TelegramBotMessages $botRequestMessage
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     * @throws \Throwable
     */
    public function checkAndProcessAnswer(
        TelegramWebhooks $webhook,
        TelegramBots $bot,
        TelegramBotScopes $scope,
        TelegramUsers $telegramUser,
        TelegramBotEvents $event,
        TelegramBotMessages $userMessage,
        TelegramBotMessages $botRequestMessage
    ) {
        /*
         * Validate inputs
         */
        $validator = \Validator::make([
            'amount' => $userMessage->message
        ], [
            'amount' => ['numeric', 'min:0.0000001'],

        ]);

        $amount = $userMessage->message;

        if ($validator->fails()) {
            $error = __('validation.enough_balance');
            $this->validationFailed($webhook, $bot, $scope, $telegramUser, $event, $error);
            return response('ok');
        }

        /*
         * Get and save info
         */
        $user = $telegramUser->user;

        if (null === $user) {
            throw new \Exception('User can not be found.');
        }

        $buy_data = cache()->get('withdraw_data'.$user->id);


        if ($buy_data==null)
        {
            $error = __('Time to withdraw has expired');
            $this->validationFailed($webhook, $bot, $scope, $telegramUser, $event, $error);
            return response('ok');
        }

        $wallet = $user->wallets()->find($buy_data['wallet_id']);
        if (null === $wallet) {
            throw new \Exception('Wallet can not be found.');
        }

        if ($wallet->balance - $wallet->balance * TransactionType::getByName('withdraw')->commission * 0.01 < $amount)
        {
            $error = __('Requested amount including withdrawal commission exceeds the wallet balance');
            $this->validationFailed($webhook, $bot, $scope, $telegramUser, $event, $error);
            return response('ok');
        }

        /*
         * Answer
         */
        $this->userAnswerSuccess($webhook, $bot, $scope, $telegramUser, $event, $userMessage, $botRequestMessage, $wallet, $amount);

        return response('ok');
    }

    /**
     * @param TelegramWebhooks $webhook
     * @param TelegramBots $bot
     * @param TelegramBotScopes $scope
     * @param TelegramUsers $telegramUser
     * @param TelegramBotEvents $event
     * @param TelegramBotMessages $userMessage
     * @param TelegramBotMessages $botRequestMessage
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     * @throws \Throwable
     */
    public function userAnswerSuccess(
        TelegramWebhooks $webhook,
        TelegramBots $bot,
        TelegramBotScopes $scope,
        TelegramUsers $telegramUser,
        TelegramBotEvents $event,
        TelegramBotMessages $userMessage,
        TelegramBotMessages $botRequestMessage,
        Wallet $wallet,
        float $amount
    ) {
        TelegramModule::setLanguageLocale($telegramUser->language);


        $user = $telegramUser->user;

        if (null === $user) {
            throw new \Exception('User can not be found.');
        }

        try {
//            Transaction::withdraw($wallet, $amount); Moved to TransactionService
            cache()->forget('withdraw_data'.$user->id);
        } catch(\Exception $e) {

            $this->validationFailed($webhook, $bot, $scope, $telegramUser, $event, $e->getMessage());
            return response('ok');
        }


        $message = view('telegram.account_bot.withdraw.user_answer_success', [
            'webhook'      => $webhook,
            'bot'          => $bot,
            'scope'        => $scope,
            'telegramUser' => $telegramUser,
            'userMessage'  => $userMessage,
        ])->render();

        if (config('app.env') == 'develop') {
            \Log::info('Prepared VIEW and message for bot:<hr>'.$message);
        }

        try {
            $telegramInstance = new TelegramModule($bot->keyword);
            $telegramInstance->sendMessage($event->chat_id,
                $message,
                'HTML',
                true,
                false,
                null,
                null,
                $scope,
                'inline_keyboard');
        } catch (\Exception $e) {
            \Log::error($e->getMessage());
            return response('ok');
        }

        TelegramBotMessages::closeUserScopes($event, $bot);

    }

    /**
     * @param TelegramWebhooks $webhook
     * @param TelegramBots $bot
     * @param TelegramBotScopes $scope
     * @param TelegramUsers $telegramUser
     * @param TelegramBotEvents $event
     * @param TelegramBotMessages $userMessage
     * @param TelegramBotMessages $botRequestMessage
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     * @throws \Throwable
     */
    public function validationFailed(
        TelegramWebhooks $webhook,
        TelegramBots $bot,
        TelegramBotScopes $scope,
        TelegramUsers $telegramUser,
        TelegramBotEvents $event,
        string $error
    ) {
        TelegramModule::setLanguageLocale($telegramUser->language);
        $message = view('telegram.account_bot.withdraw.error', [
            'webhook'      => $webhook,
            'bot'          => $bot,
            'scope'        => $scope,
            'telegramUser' => $telegramUser,
            'error' => $error
        ])->render();

        if (config('app.env') == 'develop') {
            \Log::info('Prepared VIEW and message for bot:<hr>'.$message);
        }

        try {
            $telegramInstance = new TelegramModule($bot->keyword);
            $telegramInstance->sendMessage($event->chat_id, $message, 'HTML', true);
        } catch (\Exception $e) {
            \Log::error($e->getMessage());
            return response('ok');
        }
    }
}