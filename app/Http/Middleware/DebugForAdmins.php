<?php


namespace App\Http\Middleware;


use Closure;

class DebugForAdmins
{
    public function handle($request, Closure $next)
    {

        if (user()->hasRole(['root'])) //Так у меня идет проверка админа
        {
            config()->set('app.debug', true);
        }

        return $next($request);
    }
}