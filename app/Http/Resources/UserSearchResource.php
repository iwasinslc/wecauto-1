<?php

namespace App\Http\Resources;

use App\Models\User;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @OA\Schema(
 *   title="UserSearchResource",
 *   description="User Search Resource",
 *   schema="UserSearchResource",
 *   @OA\Property(property="data", type="object", allOf={
 *     @OA\Schema(ref="#/components/schemas/User"),
 *     @OA\Schema(
 *        @OA\Property(property="partner", ref="#/components/schemas/Partner"),
 *        @OA\Property(property="licence", ref="#/components/schemas/Licence")
 *     ),
 *     @OA\Schema(ref="#/components/schemas/WalletResource"),
 *     @OA\Schema(ref="#/components/schemas/TotalResource"),
 *   })
 * )
 */
class UserSearchResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        /** @var User $this */
        return [
            'id' => $this->id,
            'name' => $this->name,
            'email' => $this->email,
            'email_verified_at' => $this->email_verified_at
                ? $this->email_verified_at->format('Y-m-d H:i:s')
                : null,
            'login' => $this->login,
            'phone' => $this->phone,
            'skype' => $this->skype,
            'google_2fa' => $this->google_2fa,
            'referral_link' => getUserReferralLink($this),
            'rank' => $this->rank->name,
            'country' => $this->country,
            'avatar' => $this->getAvatarPath(),
            'registration_date' => $this->created_at->format('Y-m-d H:i:s'),
            'partner' => $this->partner_id
                ? [
                    'id' => $this->partner_id,
                    'name' => $this->partner->name,
                    'login' => $this->partner->login,
                    'email' => $this->partner->email
                ] : null,
            'licence' => $this->licence_id
                ? new LicenceResource($this->licence) : null,
            'wallets' => WalletResource::collection($this->wallets()->with(['currency', 'paymentSystem'])->get()),
            'totals' => new TotalResource($this)
        ];
    }
}
