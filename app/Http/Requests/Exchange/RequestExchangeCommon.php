<?php
namespace App\Http\Requests\Exchange;


use App\Rules\RuleDailyLimit;
use App\Rules\RuleExchangeEnoughBalance;
use App\Rules\RuleHasLicence;
use App\Rules\RuleLicenceLimits;
use App\Rules\RuleMaxPrice;
use App\Rules\RuleMinimumSell;
use App\Rules\RuleMinPrice;
use App\Rules\RuleStandartWalletExist;
use App\Rules\RuleUUIDEqual;
use App\Rules\RuleWalletExist;
use Illuminate\Foundation\Http\FormRequest;

/**
 * Class RequestExchange
 * @package App\Http\Requests
 *
 * @property string main_wallet_id
 * @property string wallet_id
 * @property float amount
 * @property float rate
 * @property string type
 * @property string captcha
 */
class RequestExchangeCommon extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return \Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'wallet_id' => ['required', new RuleStandartWalletExist, new RuleUUIDEqual],
            'main_wallet_id' => ['required', new RuleStandartWalletExist, new RuleUUIDEqual],
            //'currency_id' => ['required', new RuleUUIDEqual],
            'rate'    => [
                'numeric',
                'required',
                new RuleMaxPrice,

                new RuleMinPrice,
            ],
            'type'=>['required', 'integer','min:0', 'max:1'],
            'amount'    => [
                'numeric',
                'required',
                new RuleHasLicence,
                new RuleLicenceLimits,
                new RuleDailyLimit,
                //new RuleHasLicence,
                //new RuleLicenceLimits,
                new RuleExchangeEnoughBalance,
                //new RuleMinimumSell,
                'min:0.00000001',
                'max:1000000'
            ],
        ];
    }

    /**
     * @return array
     */
    public function messages()
    {
        return [
            'wallet_id.required' => __('Wallet is required'),
            'main_wallet_id.required' => __('Wallet is required'),
            'amount.numeric'     => __('Amount have to be numeric'),

            
            //'captcha.required'   => trans('validation.captcha_required'),
            //'captcha.captcha'    => trans('validation.captcha_captcha'),
        ];
    }
}
