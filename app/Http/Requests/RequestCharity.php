<?php
namespace App\Http\Requests;

use App\Rules\RuleEnoughBalance;
use App\Rules\RuleStandartWalletExist;
use App\Rules\RuleUUIDEqual;
use App\Rules\RuleWalletEnoughBalance;
use App\Rules\RuleWalletExist;
use Illuminate\Foundation\Http\FormRequest;

/**
 * Class RequestWithdraw
 * @package App\Http\Requests
 *
 * @property string wallet_id
 * @property float amount
 * @property string external
 * @property string captcha
 */
class RequestCharity extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return \Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'wallet_id' => ['required', new RuleStandartWalletExist, new RuleUUIDEqual],
            'amount'    => ['numeric', new RuleWalletEnoughBalance, 'min:0.00000001', 'max:1000000'],
        ];
    }

    /**
     * @return array
     */
    public function messages()
    {
        return [
            'wallet_id.required' => __('Wallet is required'),
            'amount.numeric'     => __('Amount have to be numeric'),
        ];
    }
}
