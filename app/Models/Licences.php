<?php

namespace App\Models;

use App\Traits\ModelTrait;
use App\Traits\Uuids;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Licences
 * @package App\Models
 *
 * @property float price
 * @property string name
 * @property integer duration
 * @property integer levels
 * @property float buy_amount
 * @property float sell_amount
 * @property float deposit_min
 * @property float deposit_max
 * @property float daily_limit
 * @property string currency_id
 * @property Currency $currency
 * @property float moto_min
 * @property string created_at
 * @property string updated_at
 */



class Licences extends Model  
{
    use Uuids;
    use ModelTrait;

    public $incrementing = false;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'licences';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'price',
        'deposit_min',
        'deposit_max',
        'duration',
        'levels',
        'buy_amount',
        'sell_amount',
        'created_at',
        'currency_id',
        'moto_min',
        'daily_limit',
        'updated_at'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['created_at', 'updated_at'];


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function currency()
    {
        return $this->belongsTo(Currency::class, 'currency_id');
    }

}
