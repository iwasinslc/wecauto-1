<?php

namespace App\Models;

use App\Traits\ModelTrait;
use Illuminate\Database\Eloquent\Model;
/**
 * Class RateStatistic
 * @package App\Models
 *
 * @property integer id
 * @property float open
 * @property float close
 * @property float low
 * @property float high
 * @property string created_at
 * @property string updated_at
 */
class RateStatistic extends Model  
{
    use ModelTrait;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'rate_statistic';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['open', 'close', 'low', 'high', 'created_at', 'updated_at'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['created_at', 'updated_at'];


    public static function ratePercent()
    {
        return cache()->tags('ratePercent')->remember('ratePercent', now()->addMinutes(30), function ()  {
            $types = [
                TransactionType::getByName('exchange_buy')->id,
                TransactionType::getByName('exchange_sell')->id,
            ];

            $now = rate('ACC', 'USD');
            /**
             * @var Transaction $yest
             */
            $today = Transaction::whereIn('type_id', $types)->where('currency_id', Currency::getByCode('USD')->id)->where('created_at', '>', now()->subDay()->endOfDay()->toDateTimeString())->count();

            if ($today == 0) {
                return 0;
            }

            $yest = Transaction::whereIn('type_id', $types)->where('currency_id', Currency::getByCode('USD')->id)->where('created_at', '<=', now()->subDay()->endOfDay()->toDateTimeString())->orderBy('created_at', 'desc')->first();

            return ($now-$yest->source)/$yest->source*100;

        });




    }

}
