<?php
namespace App\Models;

use App\Traits\ModelTrait;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Reviews
 * @package App\Models
 *
 * @property string lang_id
 * @property string user_id
 * @property string name
 * @property string text
 * @property string video
 * @property integer type
 * @property integer moderated
 * @property string image
 * @property Carbon created_at
 * @property Carbon updated_at
 */
class Reviews extends Model
{
    use ModelTrait;

    /** @var array $fillable */
    protected $fillable = [
        'lang_id',
        'name',
        'text',
        'video',
        'image',
        'moderated',
        'type',
        'created_at',
        'user_id'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function lang()
    {
        return $this->belongsTo(Language::class, 'lang_id');
    }


    public function scopeActive($query)
    {
        return $query->where('moderated', 1);
    }


}
