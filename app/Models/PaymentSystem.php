<?php
namespace App\Models;

use App\Models\UserTasks\Tasks;
use App\Modules\PaymentSystems\AdvcashModule;
use App\Modules\PaymentSystems\BitcoinModule;
use App\Modules\PaymentSystems\BlockioModule;
use App\Modules\PaymentSystems\CoinpaymentsModule;
use App\Modules\PaymentSystems\CryptoCurrencyApiModule;
use App\Modules\PaymentSystems\CustomApiModule;
use App\Modules\PaymentSystems\EnpayModule;
use App\Modules\PaymentSystems\EtherApiModule;
use App\Modules\PaymentSystems\EtherApiUSDTModule;
use App\Modules\PaymentSystems\EthereumModule;
use App\Modules\PaymentSystems\FreeKassaModule;
use App\Modules\PaymentSystems\MinterApiModule;
use App\Modules\PaymentSystems\NixmoneyModule;
use App\Modules\PaymentSystems\PayeerModule;
use App\Modules\PaymentSystems\PerfectMoneyModule;
use App\Modules\PaymentSystems\PrizmApiModule;
use App\Modules\PaymentSystems\QiwiModule;
use App\Modules\PaymentSystems\VisaMastercardModule;
use App\Modules\PaymentSystems\WavesModule;
use App\Modules\PaymentSystems\WebCoinApiModule;
use App\Modules\PaymentSystems\XpayModule;
use App\Modules\PaymentSystems\YandexModule;
use App\Traits\ModelTrait;
use App\Traits\Uuids;
use Illuminate\Database\Eloquent\Model;

/**
 * Class PaymentSystem
 * @package App\Models
 *
 * @property string id
 * @property string name
 * @property string code
 * @property string instant_limit
 * @property string created_at
 * @property string updated_at
 * @property string external_balances
 * @property int connected
 * @property string minimum_topup
 * @property string minimum_withdraw
 * @property string blockchain_url
 */
class PaymentSystem extends Model
{
    use Uuids;
    use ModelTrait;

    protected $keyType = 'string';

    /** @var bool $incrementing */
    public $incrementing = false;

    /** @var array $fillable */
    protected $fillable = [
        'name',
        'id',
        'code',
        'instant_limit',
        'external_balances',
        'connected',
        'minimum_topup',
        'minimum_withdraw',
        'blockchain_url'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function currencies()
    {
        return $this->belongsToMany(Currency::class, 'currency_payment_system', 'payment_system_id', 'currency_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function transactions()
    {
        return $this->hasMany(Transaction::class, 'payment_system_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function tasks()
    {
        return $this->hasMany(Tasks::class, 'payment_system_id');
    }

    /**
     * @param string $code
     * @return PaymentSystem|null
     */
    public static function getByCode(string $code)
    {
        return cache()->tags('payment_systems')->remember($code, now()->addHour(), function() use ($code) {
            return PaymentSystem::where('code', $code)->first();
        });
    }

    /**
     * @return mixed
     * @throws \Exception
     */
    public function getClassInstance()
    {
        $pss = [
            'advcash'               => new AdvcashModule(),
            'perfectmoney'          => new PerfectMoneyModule(),
            'payeer'                => new PayeerModule(),
            'coinpayments'          => new CoinpaymentsModule(),
            'blockio'               => new BlockioModule(),
            'enpay'                 => new EnpayModule(),
            'nixmoney'              => new NixmoneyModule(),
            'waves'                 => new WavesModule(),
            'free-kassa'            => new FreeKassaModule(),
            'yandex'                => new YandexModule(),
            'qiwi'                  => new QiwiModule(),
            'visa_mastercard'       => new VisaMastercardModule(),
            'bitcoin'               => new BitcoinModule(),
            'ethereum'              => new EthereumModule(),
            'xpay'                  => new XpayModule(),
            'webcoinapi'=>new WebCoinApiModule(),
            'cryptocurencyapi'=>new CryptoCurrencyApiModule(),
            'etherapi'=>new EtherApiModule(),
            'etherapiusdt'=>new EtherApiUSDTModule(),
            'bip'=>new MinterApiModule(),
            'prizm'=>new PrizmApiModule(),
            'custom'=> new CustomApiModule()
        ];

        if (!key_exists($this->code, $pss)) {
            return false;
        }

        return $pss[$this->code];
    }



    public function getRegEx()
    {
        $pss = [
            'payeer'        => [
                'USD'=>'/^P[0-9]{6,10}$/u'
            ],
            'perfectmoney'  => [

                'USD'=>'/^U[0-9]{6,10}$/u'
            ],
            'etherapiusdt'      => [
                'USDT'=>'/^0x[a-fA-F0-9]{40}$/u'
            ],
            'etherapi'      => [
                'ETH'=>'/^0x[a-fA-F0-9]{40}$/u'
            ],
            'webcoinapi'      => [
                'WEC'=>'/^0x[a-fA-F0-9]{40}$/u'
            ],
        ];

        return (key_exists($this->code, $pss))
            ? $pss[$this->code]
            : [];
    }

    /**
     * @return array
     */
    public static function balances(): array
    {
        foreach (PaymentSystem::all() as $ps) {
            $balances[$ps->code] = json_decode($ps->external_balances, true);
        }
        return $balances ?? [];
    }

    /**
     * @return void
     */
    public static function updateBalances()
    {
        foreach (PaymentSystem::all() as $ps) {
            if ($ps->getClassInstance()) {
                $balances[$ps->code] = $ps->getClassInstance()->getBalances();
            }
        }
    }

}
