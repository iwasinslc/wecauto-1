<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class OrderPieces extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_pieces', function (Blueprint $table) {
            $table->string('id')->primary();
            $table->integer('order_id');

//            $table->string('client_pair_wallet_id');
            $table->string('main_currency_id')->nullable();
            $table->string('currency_id')->nullable();
            $table->string('main_wallet_id')->nullable();
            $table->string('wallet_id')->nullable();
            $table->decimal('rate', 24, 8)->default(0);
            $table->decimal('amount', 24, 8);
            $table->decimal('fee', 24, 8)->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_pieces');
    }
}
